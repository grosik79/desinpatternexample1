package com.sda.ed;

import com.sda.ed.com.sda.inter.ICallListener;
import com.sda.ed.com.sda.inter.IEvent;

import java.util.List;

public class CallEndedEvent implements IEvent {

    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallListener> listenerList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener listener : listenerList) {
            listener.callStarted(call_id);
        }
    }
}