package com.sda.ed.com.sda.aplication;

import java.time.LocalDateTime;

public class CallEntry {
    private int call_id;
    private LocalDateTime time_started;
    private LocalDateTime time_ended;

    public CallEntry(int call_id, LocalDateTime now) {
    }

    public int getCall_id() {
        return call_id;
    }

    public void setTime_started(LocalDateTime time_started) {
        this.time_started = time_started;
    }

    public void setTime_ended(LocalDateTime time_ended) {
        this.time_ended = time_ended;
    }
}
