package com.sda.ed.com.sda.inter;

public interface ICallListener {
    void callStarted(int call_id);
    void callEnded(int call_id);
}
