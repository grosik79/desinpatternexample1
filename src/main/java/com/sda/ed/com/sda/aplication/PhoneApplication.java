package com.sda.ed.com.sda.aplication;

import com.sda.ed.EventDispatcher;
import com.sda.ed.com.sda.inter.ICallListener;
import jdk.nashorn.internal.codegen.CompilerConstants;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class PhoneApplication implements ICallListener {
    private List<CallEntry> callRegistry = new LinkedList<>();

    public PhoneApplication() {
        EventDispatcher.instance.registerObject(this);
    }
    @Override
    public void callStarted(int call_id) {
        System.out.println(" zaincjowane połączenie" + call_id);
        for(CallEntry entry: callRegistry){
            if(entry.getCall_id() == call_id) {
                entry.setTime_started(LocalDateTime.now());
                callRegistry.add(new CallEntry(call_id, LocalDateTime.now()));
            }
        }

    }

    @Override
    public void callEnded(int call_id) {
        System.out.println(" koniec połączenia" + call_id);
        for(CallEntry entry:callRegistry){
            if(entry.getCall_id() == call_id){
                entry.setTime_ended(LocalDateTime.now());
                callRegistry.add(new CallEntry(call_id, LocalDateTime.now()));
            }
        }

    }
}
