package com.sda.ed.com.sda.inter;

public interface IEvent {
    void run();
}
