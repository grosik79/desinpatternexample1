package com.sda.ed.com.sda.aplication;

import com.sda.ed.EventDispatcher;
import com.sda.ed.com.sda.inter.ICallListener;

public class CallRecorderApplication implements ICallListener {
    public CallRecorderApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callStarted(int call_id) {

    }

    @Override
    public void callEnded(int call_id) {

    }
}
